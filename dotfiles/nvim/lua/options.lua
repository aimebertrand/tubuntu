vim.o.autochdir = true  -- auto change cwd
vim.o.path = vim.o.path .. '**'  -- set recursive file search
vim.opt.shadafile = "NONE" -- shada file for nvim
vim.o.list = true  -- show white space characters
vim.o.cursorline = true  -- highlight the current line
vim.o.title = true  -- file name in title bar
vim.o.mouse = 'a'  -- enable mouse in all modes
vim.g.visualbell = true -- only visual bell & no sound
vim.o.wildmenu = true  -- turn on the wild menu
vim.o.number = true  -- turn line numbers
vim.o.numberwidth = 4  -- space for line numbers
vim.o.clipboard = 'unnamedplus'  -- macos clipboard
vim.g.incsearch_auto_nohlsearch = 1  -- remove hl after searching
vim.o.showmatch = true  -- show matching brackets
vim.o.mat = 2  -- how long to show matching
vim.o.laststatus = 2  -- always show the status line
vim.o.termguicolors = true  -- enables 24-bit rgb
vim.o.background = 'dark' -- set dark background
vim.g.mapleader = " " -- set the leader key
vim.g.loaded_netrw = 1 -- disable netrw
vim.g.loaded_netrwPlugin = 1 -- disable netrw


-- Function to set the filetype with file patterns
local function set_the_filetype(filetype, patterns)
  vim.api.nvim_create_autocmd(
    { "BufNewFile", "BufEnter" },
    { pattern = patterns, command = "setl ft=" .. filetype }
  )
end

-- automatically set ft=bash for sh & zsh files
set_the_filetype("bash", { "*.sh", "*.zsh", "*zshrc" })
