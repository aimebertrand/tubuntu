local leader = ' '
local keymap = vim.keymap.set
local opts = { noremap = true, silent = true }


-- Files & buffers
------------------
keymap('n', leader .. '0', ':close<CR>', { noremap = true, desc = "Close current window" })
keymap('n', leader .. '1', ':only<CR>', { noremap = true, desc = "Close other windows" })
keymap('n', leader .. 's', ':w<CR>', { noremap = true, desc = "Save the buffer" })
keymap('n', leader .. 'w', ':bdel<CR>', { noremap = true, desc = "Close the buffer" })
keymap('n', leader .. ',', ':e $MYNVIMRC<CR>', { noremap = true, desc = "Open the config" })
keymap('n', leader .. ';', ':e ~/.dotfiles/nvim/lua/keybindings.lua<CR>', { noremap = true, desc = "Open keybindig config" })
keymap('n', leader .. 'q', ':qa<CR>', { noremap = true, desc = "Quit normaly" })
keymap('n', leader .. 'Q', ':qa!<CR>', { noremap = true, desc = "Force quit" })


-- Wildmenu
-----------
vim.cmd([[
cnoremap <expr> <up> pumvisible() ? "<C-p>" : "\\<up>"
cnoremap <expr> <down> pumvisible() ? "<C-n>" : "\\<down>"
]])


-- Navigation
-------------

-- tabs:
keymap('n', '<C-,>', ':tabprevious<CR>', opts)
keymap('n', '<C-.>', ':tabnext<CR>', opts)

-- splitting windows with same buffer:
keymap('n', leader .. 'ä', ':vsplit<CR><C-w><Right>', { noremap = true, desc = "Split window to the right" })
keymap('n', leader .. 'ö', ':split <CR><C-w><Down>', { noremap = true, desc = "Split window to the bottom" })

-- splitting windows with new empty buffer:
keymap('n', leader .. 'æ', ':vnew<CR><C-w>L<C-w><Right>', { noremap = true, desc = "Open new window to the right" })
keymap('n', leader .. 'œ', ':new<CR><C-w>R<C-w><Down>', { noremap = true, desc = "Open new window to the bottom" })

-- moving cursor between windows:
keymap('n', leader .. '<Right>', '<C-w><Right>', { noremap = true, desc = "Move cursor right" })
keymap('n', leader .. '<Left>', '<C-w><Left>', { noremap = true, desc = "Move cursor left" })
keymap('n', leader .. '<Down>', '<C-w><Down>', { noremap = true, desc = "Move cursor down" })
keymap('n', leader .. '<Up>', '<C-w><Up>', { noremap = true, desc = "Move cursor up" })

-- resizing windows:
keymap('n', '<M-Right>', ':vertical resize +2<CR>', opts)
keymap('n', '<M-Left>', ':vertical resize -2<CR>', opts)
keymap('n', '<M-Up>', ':resize +2<CR>', opts)
keymap('n', '<M-Down>', ':resize -2<CR>', opts)

-- Plugins
----------

-- mazy:
keymap('n', leader .. 'l', ':Lazy<CR>', { noremap = true, desc = "Pop-up Lazy" })

-- mason:
keymap('n', leader .. 'm', ':Mason<CR>', { noremap = true, desc = "Pop-up Mason" })

-- neogit:
keymap('n', leader .. 'G', ':Neogit<CR>', { noremap = true, desc = "Call NeoGit" })

-- uses telescope plugin:
keymap('n', leader .. 'b', ':Telescope buffers<CR>', { noremap = true, desc = "Switch buffer" })
keymap('n', leader .. 'c', ':Telescope colorscheme<CR>', { noremap = true, desc = "Switch color scheme" })
keymap('n', leader .. 'f', ':Telescope current_buffer_fuzzy_find<CR>', { noremap = true, desc = "Fuzzy find in buffer" })
keymap('n', leader .. 'F', ':Telescope live_grep<CR>', { noremap = true, desc = "Grep in directory" })
keymap('n', leader .. 'g', ':Telescope git_files<CR>', { noremap = true, desc = "Open git file" })
keymap('n', leader .. 'h', ':Telescope help_tags<CR>', { noremap = true, desc = "Find help tags" })
keymap('n', leader .. 'o', ':Telescope find_files<CR>', { noremap = true, desc = "Open a file" })
keymap('n', leader .. 't', ':Telescope<CR>', { noremap = true, desc = "All Telescope commands" })
keymap('n', leader .. '<Space>', ':Telescope cmdline<CR>', { noremap = true, desc = "Vim Command" })

-- floaterm:
keymap('n', leader .. 'd', ':FloatermNew  --height=0.9 --width=0.9 ranger<CR>', { noremap = true, desc = "Navigate with Ranger" })

-- easymotion:
keymap('n', 's', '<Plug>(easymotion-s)', opts)

-- colorizer:
keymap('n', leader .. 'H', ':ColorToggle<CR>', { noremap = true, desc = "Toggle colorizer" })

-- nvim-tree
keymap('n', '‚', ':NvimTreeToggle<CR>', { noremap = true, desc = "Toggle nvim tree" })
