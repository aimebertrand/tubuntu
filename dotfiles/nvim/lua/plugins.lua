local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)


local plugins = {


  -- THEMES
  'https://gitlab.com/aimebertrand/timu-caribbean-vim.git',
  'https://gitlab.com/aimebertrand/timu-spacegrey-vim.git',
  'https://gitlab.com/aimebertrand/timu-macos-vim.git',
  'https://gitlab.com/aimebertrand/timu-monokai-vim.git',
  'https://gitlab.com/aimebertrand/timu-rouge-vim.git',
  {
    "catppuccin/nvim",
    name = "catppuccin",
    priority = 1000
  },


  -- LUALINE
  {
    'nvim-lualine/lualine.nvim',
    dependencies = {
      'nvim-tree/nvim-web-devicons',
    },
    opts = {
      options = {
        icons_enabled = false,
        component_separators = {},
        section_separators = {},
        ignore_focus = {},
        always_divide_middle = true,
        globalstatus = false,
      },
      sections = {
        lualine_a = { 'mode' },
        lualine_b = { 'branch', 'diff', 'diagnostics' },
        lualine_c = { 'filename' },
        lualine_x = { 'encoding', 'fileformat', 'filetype' },
        lualine_y = { 'progress' },
        lualine_z = { 'location' }
      },
      inactive_sections = {
        lualine_a = {},
        lualine_b = {},
        lualine_c = { 'filename' },
        lualine_x = { 'location' },
        lualine_y = {},
        lualine_z = {}
      },
      tabline = {},
      winbar = {},
      inactive_winbar = {},
      extensions = {}
    }
  },


  -- TELESCOPE
  {
    'nvim-telescope/telescope.nvim',
    branch = '0.1.x',
    dependencies = {
      'nvim-lua/plenary.nvim',
      'jonarrien/telescope-cmdline.nvim',
    },
    opts =  {
      defaults = {
        layout_config = {
          height = 0.6,
          width = 0.8,
          preview_width = 0.6, -- 60% of available lines
          prompt_position = "top",
        },
        sorting_strategy = "ascending",
      },
    },
    config = function()
      require('telescope').load_extension('fzf')
    end,
  },

  {
    'nvim-telescope/telescope-fzf-native.nvim',
    build = 'make',
  },


  -- LSP AND COMPLETION
  {
    'neovim/nvim-lspconfig',
    dependencies = {
      -- automatically install lsps:
      'williamboman/mason.nvim',
      'williamboman/mason-lspconfig.nvim',
    },
  },


  -- AUTOCOMPLETION
  {
    'hrsh7th/nvim-cmp',
    dependencies = {
      'hrsh7th/cmp-nvim-lsp',
      'saadparwaiz1/cmp_luasnip',
      'L3MON4D3/LuaSnip',
    },
  },


  -- NEOGIT
  {
    "NeogitOrg/neogit",
    config = true
  },


  -- TREESITTER
  {
    'nvim-treesitter/nvim-treesitter',
    build = ':TSUpdate',
    config = function()
      require('nvim-treesitter.configs').setup {
        ensure_installed = { 'bash', 'go', 'html', 'lua', 'markdown', 'python', 'vim', 'vimdoc' },
        -- Autoinstall languages that are not installed
        auto_install = true,
        highlight = { enable = true },
        indent = { enable = true },
      }
    end,
  },


  -- WHICH-KEY
  {
    "folke/which-key.nvim",
    event = "VeryLazy",
    init = function()
      vim.o.timeout = true
      vim.o.timeoutlen = 200
    end,
    opts = {
      triggers = {"<leader>"},
      triggers_nowait = {"<leader>"}
    }
  },


  -- FLOATERM
  {
    'voldikss/vim-floaterm',
    -- For Ranger:
    config = function()
      vim.cmd([[
      let g:floaterm_opener = 'edit'
      ]])
    end
  },


 -- OTHER
  'chrisbra/colorizer',
  'easymotion/vim-easymotion',
  {
  'nvim-tree/nvim-tree.lua',
  opts = {}
  },
  'ryanoasis/vim-devicons',
  'tpope/vim-repeat',
  'tpope/vim-surround',
}

require("lazy").setup(plugins, {})
