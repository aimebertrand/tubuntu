#!/bin/bash

# FUNCTION:
# ---------
# Create icns Icon from a png file

# LICENCE:
# --------
# Copyright 2021 Aimé Bertrand

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


ICONSETNAME=$1
mkdir "${ICONSETNAME%.png}".iconset
cp "$ICONSETNAME" "${ICONSETNAME%.png}".iconset/
cd "${ICONSETNAME%.png}".iconset || exit

sips -z 16 16 "$ICONSETNAME" --out icon_16x16.png
sips -z 32 32 "$ICONSETNAME" --out icon_16x16@2x.png
sips -z 32 32 "$ICONSETNAME" --out icon_32x32.png
sips -z 64 64 "$ICONSETNAME" --out icon_32x32@2x.png
sips -z 128 128 "$ICONSETNAME" --out icon_128x128.png
sips -z 256 256 "$ICONSETNAME" --out icon_128x128@2x.png
sips -z 256 256 "$ICONSETNAME" --out icon_256x256.png
sips -z 512 512 "$ICONSETNAME" --out icon_256x256@2x.png
sips -z 512 512 "$ICONSETNAME" --out icon_512x512.png
sips -z 1024 1024 "$ICONSETNAME" --out icon_512x512@2x.png

cd ..
iconutil -c icns "${ICONSETNAME%.png}".iconset && \
rm -rf "${ICONSETNAME%.png}".iconset
