# FUNTIONS
# --------

## 1. FILE AND FOLDER MANAGEMEN#
# Create a ZIP archive of a folder:
function zipf() {
    zip -r "$1".zip "$1";
}


## 2. OPENS ANY FILE IN MACOS QUICKLOOK PREVIEW
function ql() {
    qlmanage -p "$*" >& /dev/null;
}


## 3. Get help in ZSH
function help() {
    bash -c "help $@" | nvim -c 'set ft=man'
}


## 4. DETERMINE SIZE OF A FILE OR TOTAL SIZE OF A DIRECTORY
function fs() {
    if du -b /dev/null > /dev/null 2>&1; then
    local arg=-sbh;
    else
    local arg=-sh;
    fi
    if [[ -n "$@" ]]; then
    du $arg -- "$@";
    else
    du $arg .[^.]* ./*;
    fi;
}


## 5. COLORED MANPAGES
function man() {
    env \
    LESS_TERMCAP_mb=$(printf "\e[1;31m") \
    LESS_TERMCAP_md=$(printf "\e[1;31m") \
    LESS_TERMCAP_me=$(printf "\e[0m") \
    LESS_TERMCAP_se=$(printf "\e[0m") \
    LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
    LESS_TERMCAP_ue=$(printf "\e[0m") \
    LESS_TERMCAP_us=$(printf "\e[1;36m") \
    man "$@"
}


## 6. OPEN STUFF WITH FZF
function fv() (
    export FZF_DEFAULT_COMMAND="fd -p -i -H -L -t f -t l -t x \
-E '.git/*'"
    IFS=$'\n' files=($(fzf --reverse --preview "bat --theme=timu-spacegrey --color=always {}" \
               --query="$1" --multi --select-1 --exit-0))
    [[ -n "$files" ]] && nvim "${files[@]}" # open with Emacs
)


## 7. CD WITH FZF
# fcd - including hidden directories
function fcd() {
    export FZF_DEFAULT_COMMAND="fd -p -i -H -L -t d \
-E '.git/*'"
    local dir
    dir=$(cd && fzf --reverse +m) && cd && cd "$dir"
}


## 8. BUNCH OF FUNCTION FOR LISTING
# list dot symlinks
function lsdln() {
    /bin/ls -hpGla | \
    awk '/->/ && !/\/$/ && /[0-9]+\ \./ {print $0}'
}

# list symlinks
function lsln() {
    /bin/ls -hpGla | \
    awk '/->/ && !/\/$/ && !/[0-9]+\ \./ {print $0}'
}

# list dot files
function lsdf() {
    /bin/ls -hpGla | \
    awk '!/->/ && !/\/$/ && /[0-9]+\ \./ {print $0}'
}

# list files
function lsf() {
    /bin/ls -hpGlA | \
    awk '!/->/ && !/\/$/ && !/[0-9]+\ \./ {print $0}'
}

# list dot directories
function lsdd() {
    /bin/ls -hpGlA | \
    awk '/\/$/ && /[0-9]+\ \./ {print $0}'
}

# list directories
function lsd() {
    /bin/ls -hpGlA | \
    awk '/\/$/ && !/[0-9]+\ \./ {print $0}'
}


## 10. OPEN NEW EXECUTABLE SCRIPT WITH ONE COMMAND
function csc() {
    /usr/bin/touch $1 \
    && /bin/chmod +x $1 \
    && nvim $1
}


## 11. GENERATE RANDOM PASSWORD
function genpass() {
    /usr/bin/openssl rand -base64 9
}
